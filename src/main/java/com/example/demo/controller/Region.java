package com.example.demo.controller;

import java.util.*;

public class Region {
    private Long id;//举例子 城市
    private Long parentId;//举例子 省ID
    private List<Long> nextIds;//举例子 很多个县
    public Long getId() {
        return id;
    }
    public Object getParentId() {
        return parentId;
    }
    public void setNextIds(List nextIds) {
        this.nextIds = nextIds;
    }

    public Region() {
        this.id=1l;
        this.parentId=1l;
        nextIds=new ArrayList<Long>();
        nextIds.add(2l);
        nextIds.add(3l);
    }

    public static void main(String[] args) {
        Region region=new Region();
        List<City> cityList=new ArrayList<>(); cityList.add(new City());
        List<County> countyList = new ArrayList<>();countyList.add(new County());
        List<Prov> provList = new ArrayList<>();provList.add(new Prov());
        Map<Long, List<County>> countyMap  = region.handleHierachy(cityList, countyList);//设置城市和县区的关系
        Map<Long, List<City>> cityMap   = region.handleHierachy(provList, cityList);//设置省会和城市的关系


        List<? extends Number> rtrwwt=new ArrayList<Integer>();
        List<? extends Region> rtrt=new ArrayList<County>();
        Map<Long, List<? extends Region>> ss=new HashMap<Long,List<? extends Region>>();



        Map<Long, List<County>> countyMap1   = region.handleHierachy2(cityList, countyList);//设置省会和城市的关系
        System.out.println(countyList.size()+"sdfsdf");
    }
    //upperList 不能用List<>T 因为使用到了属性   使用extends 比 T  ，可以有属性，优先使用资源更详细的
    //返回值T 如果使用属性
    //T是使用哪哪个类，属性不管
    //?可以有属性
    //返回也不能使用Map<Long, List<? extends Region>>  因为它不能add

    //Map<Long, List<? extends Region>>

    <T extends Region,M extends Region> Map<Long, List<T>> handleHierachy2(
                                                            List<M> LowerList,
                                                            List<T> upperList){
        Map<Long, List<T>> countyMap=new HashMap<>();
        for (Region c : upperList) {
            countyMap.put(c.getId(), new ArrayList<>());
        }
        for (Region co : LowerList) {
            Object parentId = co.getParentId();
            List<T> ts = countyMap.get(parentId);
            ts.add((T)co);
        }
        for (Region c : upperList) {
            c.setNextIds(countyMap.get(c.getId()));
        }
        return countyMap;
    }


    <T extends Region> Map<Long, List<T>> handleHierachy(List<? extends Region> LowerList,
               List< ? extends Region> upperList){
       Map<Long, List<T>> countyMap=new HashMap<>();
        for (Region c : upperList) {
            countyMap.put(c.getId(), new ArrayList<>());
        }
        for (Region co : LowerList) {
            Object parentId = co.getParentId();
            List<T> ts = countyMap.get(parentId);
            ts.add((T)co);
        }
        for (Region c : upperList) {
            c.setNextIds(countyMap.get(c.getId()));
        }
        return countyMap;
    }





}


//    Map<Long, List<? extends Region>> handleHierachy1(List<? extends Region> LowerList,
//                                          List< ? extends Region> upperList
//    ){
//        Map<Long, List<? extends Region>> countyMap=new HashMap<>();
//        for (Region c : upperList) {
//            countyMap.put(c.getId(), new ArrayList<>());
//        }
//        for (Region co : LowerList) {
//            Object parentId = co.getParentId();
//            List<? extends Region> ts = countyMap.get(parentId);
//            ts.add(co);
//        }
//        for (Region c : upperList) {
//            c.setNextIds(countyMap.get(c.getId()));
//        }
//        return countyMap;
//    }