package com.example.demo.controller;

import java.util.List;

public interface Common {
    List<Object> getConfig();
    void handle(List<Region> pre,List<Region> later);
}
