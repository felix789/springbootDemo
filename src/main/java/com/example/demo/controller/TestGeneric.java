package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

public class TestGeneric {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        integers.add(1);
        List<? extends Number> list=integers;  // 插入的时候不能确定是哪个number get的时候一定是number
        Number number = list.get(0);
        System.out.println(list.get(0));

        List<? super County> list2=new ArrayList<Region>();
        list2.add(new County());

        Object object = list2.get(0);//读取的时候不知道是哪个超类 插入的时候一定是country
    }

    public void add1(List<? extends Number> lis222t){
        List<? extends Number> list=new ArrayList<Integer>();//number无法知道类型 为了类型安全 只能是一个  所以不能加
        Number number1 = list.get(1);
        // Integer ddd = list.get(1);
        Number number=new Integer(2);
        //list.add("sdf");

        List<? super Integer> list2=new ArrayList<Number>();
        list2.add(new Integer(2));


    }

}
